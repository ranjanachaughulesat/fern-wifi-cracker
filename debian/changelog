fern-wifi-cracker (3.4-0kali1) kali-dev; urgency=medium

  [ Ben Wilson ]
  * Update email address
  * New helper-script format
  * Consistency with tabs to spaces
  * Remove template comment and switch spaces to tabs

  [ Sophie Brun ]
  * New upstream version 3.4
  * Refresh patches
  * Bump Standards-Version to 4.6.0

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 14 Mar 2022 17:58:05 +0100

fern-wifi-cracker (3.3-0kali2) kali-dev; urgency=medium

  [ Daniel Ruiz de Alegría ]
  * Remove xfce4-terminal from dependencies

 -- Arnaud Rebillout <arnaudr@kali.org>  Thu, 22 Jul 2021 22:55:22 +0700

fern-wifi-cracker (3.3-0kali1) kali-dev; urgency=medium

  [ Kali Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Sophie Brun ]
  * New upstream version 3.3
  * Refresh patches

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 02 Apr 2021 14:54:58 +0200

fern-wifi-cracker (3.1-0kali1) kali-dev; urgency=medium

  * New upstream version 3.1

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 15 Jun 2020 10:17:00 +0200

fern-wifi-cracker (3.0-0kali2) kali-dev; urgency=medium

  * Use x-terminal-emulator instead of xterm
  * Disable update button
  * Use dh-python
  * Remove pip usage
  * Bump Standards-Version to 4.4.1

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 13 Jan 2020 15:18:30 +0100

fern-wifi-cracker (3.0-0kali1) kali-dev; urgency=medium

  * Switch to Python 3

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 26 Sep 2019 10:14:38 +0200

fern-wifi-cracker (2.9-0kali1) kali-dev; urgency=medium

  [ Raphaël Hertzog ]
  * Update Maintainer field
  * Update Vcs-* fields for the move to gitlab.com
  * Add GitLab's CI configuration file

  [ Sophie Brun ]
  * New upstream version 2.9

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 15 Aug 2019 10:56:48 +0200

fern-wifi-cracker (2.8-0kali1) kali-dev; urgency=medium

  * Fix debian/watch
  * New upstream version 2.8
  * Use debhelper-compat 12
  * Bump Standards-Version to 4.3.0 (no changes)

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 09 Apr 2019 11:26:16 +0200

fern-wifi-cracker (2.7-0kali1) kali-dev; urgency=medium

  * Import new upstream release (bug 4963)
  * Update dependency: now requires qt5 instead of qt4

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 10 Sep 2018 14:28:12 +0200

fern-wifi-cracker (2.6-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 29 May 2018 10:34:22 +0200

fern-wifi-cracker (2.5-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 16 Feb 2017 09:52:27 +0100

fern-wifi-cracker (2.4-0kali1) kali-dev; urgency=medium

  * Imported new upstream release (Closes: 0003693)

 -- Devon Kearns <dookie@kali.org>  Tue, 08 Nov 2016 15:50:01 -0700

fern-wifi-cracker (2.3-0kali1) kali-dev; urgency=medium

  * Upstream update

 -- Mati Aharoni <muts@kali.org>  Thu, 16 Jun 2016 17:49:19 -0400

fern-wifi-cracker (2.2-0kali1) kali-dev; urgency=medium

  * Drop dependency on python-support (obsolete package)
  * Update to use debhelper 9

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 28 Jan 2016 09:56:27 +0100

fern-wifi-cracker (2.2-0kali0) kali; urgency=low

  * Imported new upstream release (Closes: 0002218)

 -- Devon Kearns <dookie@kali.org>  Fri, 17 Apr 2015 11:48:15 -0600

fern-wifi-cracker (2.1-0kali0) kali; urgency=low

  * Imported new upstream release (Closes: 0002154)

 -- Devon Kearns <dookie@kali.org>  Tue, 10 Mar 2015 19:38:33 -0600

fern-wifi-cracker (1.96-1kali1) kali; urgency=low

  * Upstream update

 -- Mati Aharoni <muts@kali.org>  Wed, 23 Jul 2014 09:02:50 -0400

fern-wifi-cracker (1.90-1kali0) kali; urgency=low

  * Imported new upstream version and disabled updates

 -- Devon Kearns <dookie@kali.org>  Wed, 12 Jun 2013 13:10:10 -0600

fern-wifi-cracker (1.86-1kali0) kali; urgency=low

  * Initial release

 -- Devon Kearns <dookie@kali.org>  Mon, 14 Jan 2013 07:31:50 -0700
